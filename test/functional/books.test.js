process.env.NODE_ENV = 'test';

var seqFixtures = require('sequelize-fixtures');
var expect = require('chai').expect;
var request = require('supertest');
var app = require('../../app.js');
var models = require("../../models/index");
var generics = require("../generic");
var config = require("../../config/tests");

var correctNewBook = {
    title: 'Niezgodna',
    publishYear: 2010,
    author: 1,
    collection: 2
};

var incorrectNewBook = {
    tile: 'Niezgodna',
    author: 1,
    collection: 2
};

describe('BOOKS', function () {

    before(function (done) {
        models.sequelize.sync({force: true}).then(function() {
            seqFixtures.loadFixtures( require('../fixtures/fixtures'), models )
                .then(function(){
                    done();
                });
        });
    });

    describe('[/api/books]', function(done){

        var root = '/api/books';

        generics.authorizationTest(root);
        generics.listTest(root, 'books', 3, ['title', 'publishYear', 'cover', 'description', 'author', '_self']);
        generics.validationBodyTests(root, correctNewBook, incorrectNewBook);
        generics.fieldsTest(root, 'book',
            {   title: 'Zbuntowana',
                publishYear: 2010,
                author: 1,
                collection: 2 },
            ['title', 'publishYear', 'cover', 'description', 'author']);
    });
    describe('[/api/books/:id]', function(done){
        it('should return 200 if id = 1', function (done) {
            request(app)
                .get('/api/books/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', 'Bearer tokenofadminm7R9MnrUotoNRtnOBZ6gyh7s2XadPNRcsYKUlCdQpSYtDCX9')
                .send()
                .expect(200)
                .end(done);
        });
        it('should return book of Tolkien when id = 1', function (done) {
            request(app)
                .get('/api/books/3')
                .set('Content-Type', 'application/json')
                .set('Authorization', 'Bearer tokenofadminm7R9MnrUotoNRtnOBZ6gyh7s2XadPNRcsYKUlCdQpSYtDCX9')
                .send()
                .expect(function(res){
                    var book = res.body.extras;
                    expect(book.author.firstName).to.equal('J.R.R');
                    expect(book.author.lastName).to.equal('Tolkien');
                })
                .end(done);
        });
        it('should not have field publishYear if id = 2', function (done) {
            request(app)
                .get('/api/books/2')
                .set('Content-Type', 'application/json')
                .set('Authorization', 'Bearer tokenofadminm7R9MnrUotoNRtnOBZ6gyh7s2XadPNRcsYKUlCdQpSYtDCX9')
                .send()
                .expect(function(res){
                    var book = res.body.extras;
                    expect(book.publishYear).not.to.exists;
                })
                .end(done);
        });
        it('should return 404 when id = 8', function (done) {
            request(app)
                .get('/api/books/8')
                .set('Content-Type', 'application/json')
                .set('Authorization', 'Bearer tokenofadminm7R9MnrUotoNRtnOBZ6gyh7s2XadPNRcsYKUlCdQpSYtDCX9')
                .send()
                .expect(404)
                .end(done);
        });
        it('should return 200 when try to edit author by admin', function(done){
            request(app)
                .put('/api/books/1', {firstName: 'Henio'})
                .set('Content-Type', 'application/json')
                .set('Authorization', 'Bearer tokenofadminm7R9MnrUotoNRtnOBZ6gyh7s2XadPNRcsYKUlCdQpSYtDCX9')
                .send()
                .expect(200)
                .end(done);
        });
        it('should return 403 when try to edit title by not creator', function(done){
            request(app)
                .put('/api/books/1', {firstName: 'Henio'})
                .set('Content-Type', 'application/json')
                .set('Authorization', 'Bearer tokenofadminm7R9MnrUotoNRtnOBZ6gyh7s2XadPNRcsYKUlCdQpSYtDCX9')
                .send()
                .expect(403)
                .end(done);
        });
        it('should return 403 when try to edit anything by user out of collection', function(done){
            request(app)
                .put('/api/books/1', {firstName: 'Henio'})
                .set('Content-Type', 'application/json')
                .set('Authorization', 'Bearer tokenofadminm7R9MnrUotoNRtnOBZ6gyh7s2XadPNRcsYKUlCdQpSYtDCX9')
                .send()
                .expect(403)
                .end(done);
        });
        it('should return 403 when try to edit collection by not an admin', function(done){
            request(app)
                .put('/api/books/1', {firstName: 'Henio'})
                .set('Content-Type', 'application/json')
                .set('Authorization', 'Bearer tokenofadminm7R9MnrUotoNRtnOBZ6gyh7s2XadPNRcsYKUlCdQpSYtDCX9')
                .send()
                .expect(403)
                .end(done);
        });
        it('should return 403 when try delete author by not admin', function(done){
            request(app)
                .delete('/api/books/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', 'Bearer tokenofuserym7R9MnrUotoNRtnOBZ6gyh7s2XadPNRcsYKUlCdQpSYtDCX8')
                .send()
                .expect(403)
                .end(done);
        });
    });
});
