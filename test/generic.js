var request = require('supertest');
var app = require('../app.js');
var config = require('../config/tests');
var expect = require('chai').expect;

/**
 * Created by andrzej on 01.10.15.
 */

module.exports = {
    authorizationTest: function(root){
        it('should return 200 with authorization header', function (done) {
            request(app)
                .get(root)
                .set('Content-Type', 'application/json')
                .set('Authorization', config.adminToken)
                .send()
                .expect(200)
                .end(done);
        });
        it('should return 401 without authorization header', function (done) {
            request(app)
                .get(root)
                .set('Content-Type', 'application/json')
                .send()
                .expect(401)
                .end(done);
        });
    },

    listTest: function(root, arrayName, count, fields){
        it('should return array with size ' + count, function (done) {
            request(app)
                .get(root)
                .set('Content-Type', 'application/json')
                .set('Authorization', config.adminToken)
                .send()
                .expect(function(res){
                    var array = res.body.extras[arrayName];
                    expect(array).to.have.length(count);
                })
                .end(done);
        });
        if(count > 0) {
            it('should return array with size ' + count, function (done) {
                request(app)
                    .get(root)
                    .set('Content-Type', 'application/json')
                    .set('Authorization', config.adminToken)
                    .send()
                    .expect(function (res) {
                        var array = res.body.extras[arrayName];
                        expect(array[0]).to.contain.all.keys(fields);
                    })
                    .end(done);
            });
        }
        it('should return pagination object', function (done) {
            request(app)
                .get(root)
                .set('Content-Type', 'application/json')
                .set('Authorization', config.adminToken)
                .send()
                .expect(function(res){
                    var pagination = res.body.extras['pagination'];
                    expect(pagination).not.to.be.undefined;
                })
                .end(done);
        });
    },

    fieldsTest: function(root, node, correctObject, fields){
        it('should return new object with fields', function (done) {
            request(app)
                .post(root)
                .set('Content-Type', 'application/json')
                .set('Authorization', config.adminToken)
                .send(correctObject)
                .expect(function(res){
                    var object = res.body.extras[node];
                    expect(object).to.contain.all.keys(fields)
                })
                .end(done);
        });
    },

    validationBodyTests: function(root, correctBody, incorrectBody){
        it('should return 201 when correct POST body', function (done) {
            request(app)
                .post(root)
                .set('Content-Type', 'application/json')
                .set('Authorization', config.adminToken)
                .send(correctBody)
                .expect(201)
                .end(done);
        });
        it('should return 400 when incorrect POST body', function (done) {
            request(app)
                .post(root)
                .set('Content-Type', 'application/json')
                .set('Authorization', config.adminToken)
                .send(incorrectBody)
                .expect(400)
                .end(done);
        });
    }
};

