/**
 * Created by andrzej on 17.09.15.
 */
module.exports = {
  preparePagination: function(root, details, next){
      var pagination = {};
      if(details.offset >= details.limit){
          var diff = +details.offset - +details.limit;
          pagination.previous = prepareRoute(root,details,diff);
      } else {
          var diff = +details.offset + +details.limit;
          pagination.next = prepareRoute(root,details,diff);
      }
      next(null, pagination)
  }
};

function prepareRoute(root, details, diff){
    var pagination = root + '?'
        + 'offset=' + diff
        + '&limit=' + details.limit;
    if(details.filter){
        pagination += '&filter=' + JSON.stringify(details.filter);
    }
    if(details.order){
        pagination += '&order=' + JSON.stringify(details.order);
    }

    return pagination;
}